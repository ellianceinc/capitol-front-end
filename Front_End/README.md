# Elliance Front End

---

## Install
This site was developed using [Middleman](https://middlemanapp.com/). Middleman has [SASS](http://sass-lang.com/), [Livereload](https://github.com/middleman/middleman-livereload), and [Autoprefixer](https://github.com/middleman/middleman-autoprefixer) included in the package.

### To Install:
- Install Ruby (if not already installed on your machine)
- `$gem install middleman`
- `cd project/path`
- `bundle install`

### Run server
- `cd project/path`
-  `bundle exec middleman`
- Visit `http://0.0.0.0:4567` or `localhost:4567`, Middleman has LiveReload built-in so the browser will refresh every time a project file is saved.

### Build Project
- `cd project/path`
-  `bundle exec middleman build`


---

## Folder Structure

Front_End  

* bin (all your Middleman goodies)

* ci-resources (more Middleman bits)

* source (this is where the magic happens, the working files)

 * assets

    * other

    * _layouts (site page templates)

    * files (PDF and other downloadable files)

    * fonts (embeded fonts and icon fonts)

    * images (all site images, Middleman knows this is the image directory so it call images using [Helper Methods](https://middlemanapp.com/basics/helper_methods/))

    * partials (all html and js parts for the site)

    * scripts (vendor scripts, and main js file. index.js.erb combines partial scripts from located in the 'partials' folder)

    * stylesheets (all styles for the site, using SASS)

 * bower-components

 * index.html.erb (the main page of your site)

 * ....html.erb (aother page of your site)

 * ....html.erb

 * ....html.erb

 * ....html.erb

 * ....html.erb

* config.rb (change Middleman settings)

* Gemfile (Middleman dependencies)
