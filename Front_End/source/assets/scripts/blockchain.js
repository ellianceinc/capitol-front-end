//= require custom/nav-offcanvas
//= require custom/search-menu

(function() {
	const   d = document;
    var     accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
	        setAccordionAria,
            switchAccordion,
            introLinks = d.querySelectorAll('.intro-link'),
            touchSupported = ('ontouchstart' in window),
            pointerSupported = ('pointerdown' in window),
            //buttonToggles = d.querySelectorAll('.rocket-buttons .buttons li a'),
            rocket = d.getElementById('blockchain');

    // Remove focus outline for users not using keyboard only
    handleFirstTab = function(e) {
        if (e.keyCode === 9) { // the "I am a keyboard user" key (tab)
            document.body.classList.add('user-is-tabbing');
            window.removeEventListener('keydown', handleFirstTab);
        }
    }
    window.addEventListener('keydown', handleFirstTab);
  
    skipClickDelay = function(e) {
        e.preventDefault();
        e.target.click();
    }

	setAriaAttr = function(el, ariaType, newProperty) {
		el.setAttribute(ariaType, newProperty);
	};
	setAccordionAria = function(el1, el2, expanded) {
		switch(expanded) {
            case "true":
                setAriaAttr(el1, 'aria-expanded', 'true');
                setAriaAttr(el2, 'aria-hidden', 'false');
                break;
            case "false":
                setAriaAttr(el1, 'aria-expanded', 'false');
                setAriaAttr(el2, 'aria-hidden', 'true');
                break;
            default:
                break;
		}
    };

    // Set focus for accessibility
    /*setFocus = function(element) {
        element.focus();
        console.log(document.activeElement);
        if (element === document.activeElement){ // Checking if the target was focused
            return false;
        } else {
            element.tabIndex = -1; // Adding tabindex for elements not focusable
            element.focus(); // Setting focus
         }
    }*/

    function scrollToItem(e) {
        e.preventDefault();
        scrollToElement = this.getAttribute('href');

        d.querySelector(scrollToElement).scrollIntoView({ 
            behavior: 'smooth' 
        });

        triggerOpen(scrollToElement);
    }
    
    // Open accordions when rocket component clicked
    switchAccordion = function(e) {
        e.preventDefault();

        var thisAnswer = e.target.parentNode.parentNode.nextElementSibling, //The expanded text
            thisQuestion = e.target.parentNode; //The link element that has been clicked

        // This is a keypress event, set the vars to accomondate for event bubbling
        if (e.target.classList.contains('js-accordionTrigger')) {
            thisAnswer = e.target.parentNode.nextElementSibling;
            thisQuestion = e.target;
        }

        //setFocus(thisQuestion);

        if (thisAnswer.classList.contains('is-collapsed')) {
            setAccordionAria(thisQuestion, thisAnswer, 'true');
            thisQuestion.scrollIntoView({ 
                behavior: 'smooth',
                block: 'start'
            });
        } else {
            setAccordionAria(thisQuestion, thisAnswer, 'false');
        }

  	    thisQuestion.classList.toggle('is-collapsed');
  	    thisQuestion.classList.toggle('is-expanded');
		thisAnswer.classList.toggle('is-collapsed');
		thisAnswer.classList.toggle('is-expanded');
        thisAnswer.classList.toggle('animateIn');
    };

    triggerOpen = function(elem) {
        elem = d.getElementById(elem.replace('#',''));

        setTimeout(function() {
            elem.querySelector('.js-accordionTrigger').click();
         }, 600);
    }
    
    // Add event listeners to rocket accordion components and bottom buttons
	for (var i=0, len=accordionToggles.length; i<len; i++) {
	    /*if (touchSupported) {
            accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
            buttonToggles[i].addEventListener('touchstart', expandAccordion, false);
        }
        if (pointerSupported) {
            accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
            buttonToggles[i].addEventListener('pointerdown', expandAccordion, false);
        }*/
        accordionToggles[i].addEventListener('click', switchAccordion, false);
        accordionToggles[i].addEventListener('keydown', function(e) {
            var key = e.which.toString();
            
            if (key.match(/13|32/)) {
                switchAccordion(e);
            }
        }, false);

        accordionToggles[i].setAttribute('data-element', [i]);
    }

    // Add event listeners for links in intro paragraph
    introLinks.forEach(link => {
        link.addEventListener('click', scrollToItem);
    });
})();