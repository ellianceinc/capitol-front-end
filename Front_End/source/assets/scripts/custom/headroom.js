//Headroom.js
$("document").ready(function(){
  var myElement = document.querySelector("#header");
  var headroom  = new Headroom(myElement,{
      offset: 75,
      tolerance : {
          up : 20,
          down : 0
      }
    });
  headroom.init();
});



$("document").ready(function(){
  var myElement = document.querySelector("#alert");
  var headroom  = new Headroom(myElement);
  headroom.init();
});
