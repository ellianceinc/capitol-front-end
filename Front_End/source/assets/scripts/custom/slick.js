//Slick -

//Homepage - Hero
$(document).ready(function() {

	//Text Portion - Options
	$(".homepage-hero--txt").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: ".homepage-hero--img",
		arrows: false,
		dots: true,
		pauseOnHover: false,
		pauseOnDotsHover: false,
		adaptiveHeight: true,
		regionLabel: 'A Carousel of Highlights from Captech U',
		responsive: [
			{
				breakpoint: 760,
				settings: {
					adaptiveHeight: false
				}
			}
		]
	});

	//Image Portion
	$(".homepage-hero--img").slick({
		autoplay: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: ".homepage-hero--txt",
		fade: true,
		autoplaySpeed: 4000,
		pauseOnHover: false,
		pauseOnDotsHover: false,
		draggable: false,
		adaptiveHeight: true,
		useAutoplayToggleButton: false,
		responsive: [
			{
				breakpoint: 760,
				settings: {
					draggable: true,
					fade: false
				}
			}
		]
	});

	$(".homepage-hero--play-pause").on("click", function() {
		if ($(this).hasClass('pause')) {
			$(".homepage-hero--img").slick('slickPause');
			$(this).removeClass('pause');
			$(this).addClass('play')
			$(this).html('<i class="zmdi zmdi-play"></i>');
			$(this).attr('aria-label', 'Play the carousel');
		}
		else {
			$(".homepage-hero--img").slick('slickPlay');
			$(this).removeClass('play');
			$(this).addClass('pause')
			$(this).html('<i class="zmdi zmdi-pause"></i>');
			$(this).attr('aria-label', 'Pause the carousel');
		}
	});
});


// Homepage - News Feed
$(document).ready(function() {

	// $(".homepage-feed").on("init reInit afterChange", function(
	// 	event,
	// 	slick,
	// 	currentSlide,
	// 	nextSlide
	// ) {
	// 	// $(".homepage-feed a").attr('tabindex', -1);
	// 	$(".homepage-feed .slick-slide").attr('tabindex', -1); 
	// 	$(".homepage-feed .slick-slide.slick-active").attr('tabindex', -1); 
	// });

	$(".homepage-feed").slick({
		accessibility: true,
		regionLabel: 'A Carousel highlighting News, Events and Blog posts',
		infinite: false,
		slidesToShow: 3.25,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		pauseOnHover: false,
		pauseOnDotsHover: false,
		adaptiveHeight: false,
		nextArrow: '.homepage-feed--next',
		prevArrow: '.homepage-feed--prev',
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2.25,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 920,
				settings: {
					slidesToShow: 1.25,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1.15,
					slidesToScroll: 1
				}
			}
		]
	});
});





// Carousel Double
$(document).ready(function() {
	//Text Portion
	var $status = $(".pagingInfo");
	var $slickElement = $(".carousel-double--txt");

	$slickElement.on("init reInit afterChange", function(
		event,
		slick,
		currentSlide,
		nextSlide
	) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$status.text(i + "/" + slick.slideCount);
	});

	//Text Portion - Options
	$slickElement.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: ".carousel-double--img",
		arrows: false,
		dots: false,
		pauseOnHover: false,
		pauseOnDotsHover: false,
		adaptiveHeight: true,
		regionLabel: 'A Carousel of text blocks that corresponds with the image carousel',
	});

	//Image Portion
	$(".carousel-double--img").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		asNavFor: ".carousel-double--txt",
		autoplay: false,
		autoplaySpeed: 3000,
		pauseOnHover: false,
		pauseOnDotsHover: false,
		nextArrow: '.carousel-double--next',
		prevArrow: '.carousel-double--prev',
		adaptiveHeight: true,
		regionLabel: 'A Carousel of images',
	});
});

$(document).ready(function() {
	$(".faculty-carousel").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<button class="slick-previous"><svg class="arrow-prev" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow"></path></svg><span class="screen-reader-text">Previous</span></button>',
		nextArrow: '<button class="slick-next"><svg class="arrow-next" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg><span class="screen-reader-text">Next Slide</span></button>',
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 760,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 920,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			}
		]
	});
});

$(document).ready(function() {
	$(".alumni-testimonials-carousel").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button class="slick-previous"><svg class="arrow-prev" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow"></path></svg><span class="screen-reader-text">Previous</span></button>',
		nextArrow: '<button class="slick-next"><svg class="arrow-next" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg><span class="screen-reader-text">Next Slide</span></button>',

	});
});
