(function() {
    const menuBase = document.getElementById('nav--full');
    const menuClose = document.getElementById('nav--close');
    const menuOpen = document.getElementById('nav--open');
    const menuDialog = document.getElementById('nav--full-dialog');
    const menuDialogInner = menuDialog.querySelector('.nav--full-dialog-inner');
    const l1Buttons =  document.querySelectorAll('.level-1--btn');
    const l2Uls = document.querySelectorAll('.level-2--ul');

    let index = 0;

    const setFocus = (newIndex, focus = false) => {
        currentButton = l1Buttons[index];
        newButton = l1Buttons[newIndex];

        currentButton.setAttribute('aria-expanded', false);
        currentButton.expanded = false;
        currentButton.controls.classList.remove('current');

        newButton.setAttribute('aria-expanded', true);
        newButton.expanded = true;
        newButton.controls.classList.add('current');
        if (focus) { newButton.focus(); }

        index = newIndex;

    }

    menuClose.addEventListener('click', (e) => {
        menuDialogInner.classList.remove('open');
        window.setTimeout(() => {
            // menuBase.classList.remove('open');
            menuDialog.close();
            document.documentElement.classList.remove('disable-scroll');
        }, 75);
        
    });

    menuOpen.addEventListener('click', (e) => {
        // menuBase.classList.add('open');
        menuDialog.showModal();
        document.documentElement.classList.add('disable-scroll');
        window.setTimeout(() => {
            menuDialogInner.classList.add('open');
        }, 75);
    });

    l1Buttons.forEach((button, i) => {
        // let controlsId = button.getAttribute('aria-controls');
        button.controls = document.getElementById(button.getAttribute('aria-controls'));
        button.expanded = button.getAttribute('aria-expanded') === 'true';
        button.addEventListener('click', (e) => {
            if (button.expanded) { return; }
            setFocus(i);
        });
    });
})();