//Set "Questions?" RFI button
// $(document).ready(function () {
//     // get faculty text and link
//     var contactText = "Questions? " + $('a#key-faculty')[0].innerText;
//     var contactLink = "/faculty-contact" + $('a#key-faculty')[0].search;
  
//     // set button text and link
//     $('a#key-faculty-request').text(contactText);
//     $('a#key-faculty-request').attr("href", contactLink);
//   });
  
//   // Autosubmit Alumni Profiles form
//   $(document).ready(function () {
//     $(document).on('change', '.view-alumni-profiles .views-exposed-form select', function (e) {
//       $target = $(e.target);
//       $target.closest('form').find('.form-submit').click();
//     });
//   });

//Set "Questions?" RFI button
(function() {
  console.log("Attempting to set key-faculty-request button text and link");
  const keyFacultySource = document.getElementById('key-faculty'),
        keyFacultyRequest = document.getElementById('key-faculty-request');
  if (keyFacultySource !== null && keyFacultyRequest !== null) {
    keyFacultyRequest.textContent = `Questions? ${keyFacultySource.textContent}`;
    keyFacultyRequest.setAttribute('href', keyFacultySource.getAttribute('href'));
    console.log("Key faculty exists: ", keyFacultySource.getAttribute('href'));
  } else {
    // default to admitadmin
    console.log("No key faculty, set a default value");
    keyFacultyRequest.textContent = 'Questions? Contact Carmit Levin';
    const relativePath = window.location.pathname;
    const currentPagePath = `/faculty-contact?fac_id=12461&page=${relativePath}`
    keyFacultyRequest.setAttribute('href', `${currentPagePath}`);
  }
})();

(function() {
  const select = document.querySelector('.view-alumni-profiles .views-exposed-form select');
  if (select !== null) {
    const form = select.closest('form');
    select.addEventListener('change', (e) => {
      form.submit();
    });  
  }
})();

(function() {
  const submit = document.querySelector('form#webform-submission-capitoltech-professional-educati-node-2901-add-form > input#edit-actions-submit');
  if (submit !== null) {
    submit.addEventListener('click', (e) => {
      const input = document.querySelector('input[name=merchant_reference_number]');
      let d = new Date();
      input.value = `ref${d.getTime()}`;
    });
  }
})();