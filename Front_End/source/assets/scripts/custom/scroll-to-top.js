//Scroll to top link
// var $toplink = $('.back-to-top');
// $toplink.click(function() {
//     $('html, body').animate({
//         scrollTop: $('body').offset().top
//     }, 500);
// });

(function() {
    const topLink = document.querySelector('.back-to-top');
    topLink.addEventListener('click', (e) => {
        window.scrollTo({
            top: document.body.offsetTop,
            left: 0,
            behavior: 'smooth',
        })
    });
})();