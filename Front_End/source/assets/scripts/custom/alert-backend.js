//look for alerts on every page load
(function() {
  const checkCookie = (cname, cval) => {
    cname = `${cname}=`;
    carray = document.cookie.split(';');
    return carray.reduce((match, c) => {
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }
      return ((c.indexOf(cname) == 0) && (c.substring(cname.length, c.length) === cval)) ? true : match;
    }, false);
  }

  const setCookie = (cname, cval, end) => {
    document.cookie = `${cname}=${cval}`;
  }

  window.addEventListener('load', async (e) =>{
    try {
      const base_url_api_path = `//${window.location.host}/api/alert.json?_format=json`;
      const response = await fetch(base_url_api_path);
      const data = await response.json();
  
      const now = new Date();
      const begin = new Date(data[0].field_alert_begins[0].value);
      const end = new Date(data[0].field_alert_ends[0].value);

      const cname = 'ctu-alert';
  
      if (checkCookie(cname, data[0].uuid[0].value)) {
        return;
      };
 
      if (now > begin && now < end) {
        let alert_div = document.createElement('div');
        alert_div.id = 'alert';
        alert_div.classList.add('active');

        let link_string = '';
        if (data[0].field_link[0] && data[0].field_link[0].uri) {
          link_string = `<br/><a href="${data[0].field_link[0].uri}">${data[0].field_link[0].title}</a>`;
        }
    
        alert_div.innerHTML = `<div class="alert-inner">` + 
          `<p><strong>${data[0].field_alert_title[0].value}:</strong> ${data[0].field_alert_text[0].value} ${link_string}</p>` +
          `<button id="alert--dismiss"><span class="screen-reader-text">Dismiss</span><span class="zmdi zmdi-close-circle"></span></button></div>`;
        
        document.body.prepend(alert_div);
        
        const dismiss = document.getElementById('alert--dismiss');
        dismiss.addEventListener('click', (e) => {
          alert_div.classList.remove('active');
          setCookie(cname, data[0].uuid[0].value, end);
        });

      }
    } catch(error) {
      console.error(error);
    }
  });
})();
// $(document).ready(async function () {
//   try {
//     const base_url_api_path = '//' + $(location).attr('host') + '/api/alert.json?_format=json';
//     const response = await fetch(base_url_api_path);
//     const data = await response.json();

//     const $now = $.now();
//     const $begin = new Date(data[0].field_alert_begins[0].value);
//     const $end = new Date(data[0].field_alert_ends[0].value);

//     let $link_string = '';
//     if (data[0].field_link[0] && data[0].field_link[0].uri) {
//       $link_string = `<br/><a href="${data[0].field_link[0].uri}">${data[0].field_link[0].title}</a></p>`;
//     }

//     let $html = `<div id="alert" class="active">
//       <div class="alert-inner">
//         <p><strong>${data[0].field_alert_title[0].value}: </strong>
//         ${data[0].field_alert_text[0].value}
//         ${$link_string}
//         <a id="alert--dismiss">
//           <span class="screen-reader-text">Dismiss</span>
//           <span class="zmdi zmdi-close-circle"></span>
//         </a>
//       </div>
//     </div>`;

//     if ($now > $begin.getTime() && $now < $end.getTime()) {
//       $('body').prepend($html);
//       await new Promise(resolve => setTimeout(resolve, 0)); // Allow rendering before triggering resize
//       console.log('********** alert rendered ***************');
//       window.dispatchEvent(new Event('resize'));
//       // Add dismiss logic
//       $(document).on('click', 'body a#alert--dismiss', function () {
//         $('body').find('div#alert').removeClass('active');
//       });
//     }
//   } catch (err) {
//     // Handle errors
//     console.error(err);
//   }

  
// });

  
  // Faculty bio
  // $('button.profile-row--trigger').click(function() {
  //   console.log("****************** CLICK ******************");
  // }