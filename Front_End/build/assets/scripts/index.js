//http://web-accessibility.carnegiemuseums.org/code/accordions/
(function() {
  const accordionButtons = document.querySelectorAll('.accordion-controls li button');

  accordionButtons.forEach((button) => {
    button.content = document.getElementById(button.getAttribute('aria-controls'));
    button.expanded = button.getAttribute('aria-expanded') === 'true';
    button.toggleVal = () => {
      button.expanded = !button.expanded;
      button.setAttribute('aria-expanded', button.expanded);
      button.toggleMarkup();
    };
    button.toggleMarkup = () => {
      if (button.expanded) {
        button.content.style.setProperty('display', 'block');
      }
      else {
        button.content.style.setProperty('display', 'none');
      }
    }
    button.toggleMarkup();
    button.addEventListener('click', button.toggleVal);
  });

})();
// var accordionButtons = $('.accordion-controls li button');

// function accordionToggle() {
//   $('.accordion-controls li button').on('click', function(e) {
//     $control = $(this);

//     accordionContent = $control.attr('aria-controls');
//     // checkOthers($control[0]);

//     isAriaExp = $control.attr('aria-expanded');
//     newAriaExp = (isAriaExp == "false") ? "true" : "false";
//     $control.attr('aria-expanded', newAriaExp);

//     isAriaHid = $('#' + accordionContent).attr('aria-hidden');
//     if (isAriaHid == "true") {
//       $(this).next('div').attr('aria-hidden', "false");
//       $(this).next('div').css('display', 'block');
//     } else {
//       $(this).next('div').attr('aria-hidden', "true");
//       $(this).next('div').css('display', 'none');
//     }
//   });
// };


// //call this function on page load
// accordionToggle();


// //close accorions by default
// function checkInitialState() {
//   for (var i=0; i<accordionButtons.length; i++) {
//     if (($(accordionButtons[i]).attr('aria-expanded')) == 'false') {
//       $(accordionButtons[i]).next('div').css('display', 'none');
//     }
//   }
// };
// checkInitialState();
///////////////////////Tabs///////////////////////
// $(function() {
//     var index = 0;
//     var $tabs = $('button.tabOLD');
  
//     $tabs.bind ({
//       // on keydown,
//       // determine which tab to select
//       keydown: function(ev){
//         var LEFT_ARROW = 37;
//         var UP_ARROW = 38;
//         var RIGHT_ARROW = 39;
//         var DOWN_ARROW = 40;
  
//         var k = ev.which || ev.keyCode;
  
//         // if the key pressed was an arrow key
//         if (k >= LEFT_ARROW && k <= DOWN_ARROW){
//           // move left one tab for left and up arrows
//           if (k == LEFT_ARROW || k == UP_ARROW){
//             if (index > 0) {
//               index--;
//             }
//             // unless you are on the first tab,
//             // in which case select the last tab.
//             else {
//               index = $tabs.length - 1;
//             }
//           }
  
//           // move right one tab for right and down arrows
//           else if (k == RIGHT_ARROW || k == DOWN_ARROW){
//             if (index < ($tabs.length - 1)){
//               index++;
//             }
//             // unless you're at the last tab,
//             // in which case select the first one
//             else {
//               index = 0;
//             }
//           }
  
//           // trigger a click event on the tab to move to
//           $($tabs.get(index)).click();
//           ev.preventDefault();
//         }
//       },
  
//       // just make the clicked tab the selected one
//       click: function(ev) {
//         ev.preventDefault();
//         index = $.inArray(this, $tabs.get());
//         setFocus();
//       }
//     });
  
//     var setFocus = function(){
//       // undo tab control selected state,
//       // and make them not selectable with the tab key
//       // (all tabs)
//       $tabs.attr(
//       {
//         tabindex: '-1',
//         'aria-selected': 'false'
//       }).removeClass('selected');
  
//       // hide all tab panels.
//       $('.tab-panel').removeClass('current');
  
//       // make the selected tab the selected one, shift focus to it
//       $($tabs.get(index)).attr(
//       {
//         tabindex: '0',
//         'aria-selected': 'true'
//       }).addClass('selected').focus();
  
//       // handle parent <li> current class (for coloring the tabs)
//       $($tabs.get(index)).parent().siblings().removeClass('current');
//       $($tabs.get(index)).parent().addClass('current');
  
//       // add a current class also to the tab panel
//       // controlled by the clicked tab
//       $($($tabs.get(index)).attr('data-target')).addClass('current');
//     };
//   });
  
  (function() {
    let index = 0;
    const arrows = {
      left: 37,
      up: 38,
      right: 39,
      down: 40
    };
    const tabs = document.querySelectorAll('button.tab');

    const setFocus = (newIndex, focus = false) => {
      currentTab = tabs[index];
      currentPanel = document.getElementById(currentTab.getAttribute('aria-controls'));
      newTab = tabs[newIndex];
      newPanel = document.getElementById(newTab.getAttribute('aria-controls'));

      currentTab.setAttribute('tabindex', '-1');
      currentTab.setAttribute('aria-selected', 'false');
      currentTab.classList.remove('selected');
      currentPanel.classList.remove('current');

      newTab.setAttribute('tabindex', '0');
      newTab.setAttribute('aria-selected', 'true');
      newTab.classList.add('selected');
      if (focus) { newTab.focus(); }
      newPanel.classList.add('current');

      index = newIndex;
    }

    tabs.forEach((tab, i) => {
      tab.addEventListener('click', (e) => {
        setFocus(i);
      });
      tab.addEventListener('keydown', (e) => {
        const k = e.which || e.keyCode;
        console.log(Object.values(arrows).includes(k));
  
        if (Object.values(arrows).includes(k)){
          e.preventDefault();
          let newIndex;
          if (k == arrows.left || k == arrows.up){
            newIndex = (index > 0) ? index - 1 : tabs.length - 1;
          }  
          else if (k == arrows.right || k == arrows.down){
            newIndex =  (index < (tabs.length - 1)) ? index + 1 : 0;
          }
          setFocus(newIndex, true);
        }
      });
    });
  })();
(function() {
    const menuBase = document.getElementById('nav--full');
    const menuClose = document.getElementById('nav--close');
    const menuOpen = document.getElementById('nav--open');
    const menuDialog = document.getElementById('nav--full-dialog');
    const menuDialogInner = menuDialog.querySelector('.nav--full-dialog-inner');
    const l1Buttons =  document.querySelectorAll('.level-1--btn');
    const l2Uls = document.querySelectorAll('.level-2--ul');

    let index = 0;

    const setFocus = (newIndex, focus = false) => {
        currentButton = l1Buttons[index];
        newButton = l1Buttons[newIndex];

        currentButton.setAttribute('aria-expanded', false);
        currentButton.expanded = false;
        currentButton.controls.classList.remove('current');

        newButton.setAttribute('aria-expanded', true);
        newButton.expanded = true;
        newButton.controls.classList.add('current');
        if (focus) { newButton.focus(); }

        index = newIndex;

    }

    menuClose.addEventListener('click', (e) => {
        menuDialogInner.classList.remove('open');
        window.setTimeout(() => {
            // menuBase.classList.remove('open');
            menuDialog.close();
            document.documentElement.classList.remove('disable-scroll');
        }, 75);
        
    });

    menuOpen.addEventListener('click', (e) => {
        // menuBase.classList.add('open');
        menuDialog.showModal();
        document.documentElement.classList.add('disable-scroll');
        window.setTimeout(() => {
            menuDialogInner.classList.add('open');
        }, 75);
    });

    l1Buttons.forEach((button, i) => {
        // let controlsId = button.getAttribute('aria-controls');
        button.controls = document.getElementById(button.getAttribute('aria-controls'));
        button.expanded = button.getAttribute('aria-expanded') === 'true';
        button.addEventListener('click', (e) => {
            if (button.expanded) { return; }
            setFocus(i);
        });
    });
})();
//look for alerts on every page load
(function() {
  const checkCookie = (cname, cval) => {
    cname = `${cname}=`;
    carray = document.cookie.split(';');
    return carray.reduce((match, c) => {
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }
      return ((c.indexOf(cname) == 0) && (c.substring(cname.length, c.length) === cval)) ? true : match;
    }, false);
  }

  const setCookie = (cname, cval, end) => {
    document.cookie = `${cname}=${cval}`;
  }

  window.addEventListener('load', async (e) =>{
    try {
      const base_url_api_path = `//${window.location.host}/api/alert.json?_format=json`;
      const response = await fetch(base_url_api_path);
      const data = await response.json();
  
      const now = new Date();
      const begin = new Date(data[0].field_alert_begins[0].value);
      const end = new Date(data[0].field_alert_ends[0].value);

      const cname = 'ctu-alert';
  
      if (checkCookie(cname, data[0].uuid[0].value)) {
        return;
      };
 
      if (now > begin && now < end) {
        let alert_div = document.createElement('div');
        alert_div.id = 'alert';
        alert_div.classList.add('active');

        let link_string = '';
        if (data[0].field_link[0] && data[0].field_link[0].uri) {
          link_string = `<br/><a href="${data[0].field_link[0].uri}">${data[0].field_link[0].title}</a>`;
        }
    
        alert_div.innerHTML = `<div class="alert-inner">` + 
          `<p><strong>${data[0].field_alert_title[0].value}:</strong> ${data[0].field_alert_text[0].value} ${link_string}</p>` +
          `<button id="alert--dismiss"><span class="screen-reader-text">Dismiss</span><span class="zmdi zmdi-close-circle"></span></button></div>`;
        
        document.body.prepend(alert_div);
        
        const dismiss = document.getElementById('alert--dismiss');
        dismiss.addEventListener('click', (e) => {
          alert_div.classList.remove('active');
          setCookie(cname, data[0].uuid[0].value, end);
        });

      }
    } catch(error) {
      console.error(error);
    }
  });
})();
// $(document).ready(async function () {
//   try {
//     const base_url_api_path = '//' + $(location).attr('host') + '/api/alert.json?_format=json';
//     const response = await fetch(base_url_api_path);
//     const data = await response.json();

//     const $now = $.now();
//     const $begin = new Date(data[0].field_alert_begins[0].value);
//     const $end = new Date(data[0].field_alert_ends[0].value);

//     let $link_string = '';
//     if (data[0].field_link[0] && data[0].field_link[0].uri) {
//       $link_string = `<br/><a href="${data[0].field_link[0].uri}">${data[0].field_link[0].title}</a></p>`;
//     }

//     let $html = `<div id="alert" class="active">
//       <div class="alert-inner">
//         <p><strong>${data[0].field_alert_title[0].value}: </strong>
//         ${data[0].field_alert_text[0].value}
//         ${$link_string}
//         <a id="alert--dismiss">
//           <span class="screen-reader-text">Dismiss</span>
//           <span class="zmdi zmdi-close-circle"></span>
//         </a>
//       </div>
//     </div>`;

//     if ($now > $begin.getTime() && $now < $end.getTime()) {
//       $('body').prepend($html);
//       await new Promise(resolve => setTimeout(resolve, 0)); // Allow rendering before triggering resize
//       console.log('********** alert rendered ***************');
//       window.dispatchEvent(new Event('resize'));
//       // Add dismiss logic
//       $(document).on('click', 'body a#alert--dismiss', function () {
//         $('body').find('div#alert').removeClass('active');
//       });
//     }
//   } catch (err) {
//     // Handle errors
//     console.error(err);
//   }

  
// });

  
  // Faculty bio
  // $('button.profile-row--trigger').click(function() {
  //   console.log("****************** CLICK ******************");
  // }
;
///////////////////////Site Search Dropdown///////////////////////

(function() {
  const searchTrigger = document.getElementById('search--trigger'),
        searchDiv = document.querySelector('.search--site-search');
  
  function closeSearch() {
    searchTrigger.classList.remove('open');
    searchTrigger.setAttribute('aria-label', 'Open search input');
    searchDiv.classList.remove('search-open');
    window.removeEventListener('click', onDocumentClick);
  }

  const disableFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      element.setAttribute('tabindex', -1);
    });
  }

  const onDocumentClick = (e) => {
    if (!e.target.closest('.search--site-search')) {
      closeSearch();
    }
  }

  const toggleFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      let tabindex = (element.getAttribute('tabindex') === 0 ) ? -1 : 0;
      element.setAttribute('tabindex', tabindex);
    });
  }

  searchTrigger.addEventListener('click', (e) => {
    if (searchTrigger.classList.contains('open')) {
      closeSearch();
    }
    else {
      searchTrigger.classList.add('open');
      searchTrigger.setAttribute('aria-label', 'Close search input');
      searchDiv.classList.add('search-open');
      toggleFocusableElements();
      window.addEventListener('click', onDocumentClick);
    }
  });
})();
//Scroll to top link
// var $toplink = $('.back-to-top');
// $toplink.click(function() {
//     $('html, body').animate({
//         scrollTop: $('body').offset().top
//     }, 500);
// });

(function() {
    const topLink = document.querySelector('.back-to-top');
    topLink.addEventListener('click', (e) => {
        window.scrollTo({
            top: document.body.offsetTop,
            left: 0,
            behavior: 'smooth',
        })
    });
})();
//Set "Questions?" RFI button
// $(document).ready(function () {
//     // get faculty text and link
//     var contactText = "Questions? " + $('a#key-faculty')[0].innerText;
//     var contactLink = "/faculty-contact" + $('a#key-faculty')[0].search;
  
//     // set button text and link
//     $('a#key-faculty-request').text(contactText);
//     $('a#key-faculty-request').attr("href", contactLink);
//   });
  
//   // Autosubmit Alumni Profiles form
//   $(document).ready(function () {
//     $(document).on('change', '.view-alumni-profiles .views-exposed-form select', function (e) {
//       $target = $(e.target);
//       $target.closest('form').find('.form-submit').click();
//     });
//   });

//Set "Questions?" RFI button
(function() {
  console.log("Attempting to set key-faculty-request button text and link");
  const keyFacultySource = document.getElementById('key-faculty'),
        keyFacultyRequest = document.getElementById('key-faculty-request');
  if (keyFacultySource !== null && keyFacultyRequest !== null) {
    keyFacultyRequest.textContent = `Questions? ${keyFacultySource.textContent}`;
    keyFacultyRequest.setAttribute('href', keyFacultySource.getAttribute('href'));
    console.log("Key faculty exists: ", keyFacultySource.getAttribute('href'));
  } else {
    // default to admitadmin
    console.log("No key faculty, set a default value");
    keyFacultyRequest.textContent = 'Questions? Contact Carmit Levin';
    const relativePath = window.location.pathname;
    const currentPagePath = `/faculty-contact?fac_id=12461&page=${relativePath}`
    keyFacultyRequest.setAttribute('href', `${currentPagePath}`);
  }
})();

(function() {
  const select = document.querySelector('.view-alumni-profiles .views-exposed-form select');
  if (select !== null) {
    const form = select.closest('form');
    select.addEventListener('change', (e) => {
      form.submit();
    });  
  }
})();

(function() {
  const submit = document.querySelector('form#webform-submission-capitoltech-professional-educati-node-2901-add-form > input#edit-actions-submit');
  if (submit !== null) {
    submit.addEventListener('click', (e) => {
      const input = document.querySelector('input[name=merchant_reference_number]');
      let d = new Date();
      input.value = `ref${d.getTime()}`;
    });
  }
})();







