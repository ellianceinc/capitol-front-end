///////////////////////Nav Offcanvas///////////////////////
//http://web-accessibility.carnegiemuseums.org/code/accordions/
/*jslint browser: true*/
/*global $, jQuery, alert*/


var navButtons = $('#nav--main li button');

function navToggle() {
  // setting the correct active flyout menu item.
  var $activenavs;
  if (window.location.pathname === "/") {
    $activenavs = $('#nav--full #nav--main > li:nth-child(1) > ul > li:nth-child(1) > a');
  } else {
    $activenavs = $('#nav--full li a[href*="' + window.location.pathname + '"], #nav--full div.callouts a[href*="' + window.location.pathname + '"]');
  }
  $activenavs.each(function(idx, el) {
    $el = $(el);
    $el.addClass('current-page');
    var $mainnavParent = $el.parents('#nav--main > li');
    if ($mainnavParent[0]) {
      var $mainnavParentBtn = $($mainnavParent.find('button'));
      $mainnavParentBtn.attr('aria-expanded', true);
      $mainnavParentBtn.next('ul').css('display', 'block');
      $mainnavParentBtn.next('ul').attr('aria-hidden', "false");
      checkNavOthers($mainnavParentBtn[0]);
    }
  });
  // add the button click support
  $('#nav--main li button').on('click', function(e) {
    $control = $(this);

    navContent = $control.attr('aria-controls');
    checkNavOthers($control[0]);

    isAriaExp = $control.attr('aria-expanded');
    newAriaExp = (isAriaExp == "false") ? "true" : "false";
    $control.attr('aria-expanded', newAriaExp);

    isAriaHid = $('#' + navContent).attr('aria-hidden');
    if (isAriaHid == "true") {
      $(this).next('ul').attr('aria-hidden', "false");
      $(this).next('ul').css('display', 'block');
    } else {
      $(this).next('ul').attr('aria-hidden', "true");
      $(this).next('ul').css('display', 'none');
    }
  });
};


//close if others are open and close them
function checkNavOthers(elem) {
  for (var i = 0; i < navButtons.length; i++) {
    if (navButtons[i] != elem) {
      if (($(navButtons[i]).attr('aria-expanded')) == 'true') {
        $(navButtons[i]).attr('aria-expanded', 'false');
        content = $(navButtons[i]).attr('aria-controls');
        $('#' + content).attr('aria-hidden', 'true');
        $('#' + content).css('display', 'none');
        $('#' + content).parent().removeClass('active');
      }
    }
  }
};


//call this function on page load
$(document).ready(function() {
  navToggle();
});


//close accorions by default
function checkInitialNavState() {
  for (var i = 0; i < navButtons.length; i++) {
    if (($(navButtons[i]).attr('aria-expanded')) == 'false') {
      $(navButtons[i]).next('ul').css('display', 'none');
    }
  }
};
checkInitialNavState();









//Menu Open/Close button

//Toggle Scrip
$('.trigger').click(function() {
  var $this = $(this),
    notThis = $this.hasClass('open'),
    $thisNav = $('#' + $this.attr("rel"));

  //open the nav
  $this.toggleClass('open');
  $thisNav.toggleClass('open');
  if ($(this).hasClass("block-scroll")) {
    $('html').toggleClass('disable-scroll');
    $('#overlay-mobile').toggleClass('visible');
  }
  $toggleTIElements = $thisNav.find('.toggle-tabindex');
  $toggleTIElements.attr('tabindex', function(index, currentTI) {
    return ((currentTI - 1) % 2) + 0;
  });
  if ($(this).hasClass('open')) {
    $(this).find('.screen-reader-text').text('Close menu');
    //add event listener
    $(document).on('keydown', {trigger: $this, nav: $thisNav}, navOffcanvasTrapFocus);
  }
  else {
    $(this).find('.screen-reader-text').text('Open menu');
    //remove event listener
    $(document).off('keydown', navOffcanvasTrapFocus);
  }
});
//Close button
$('.offcanvas-close-button').click(function() {
  $('.triggered-area, .trigger').removeClass('open');
  $('html').removeClass('disable-scroll');
  $('#overlay-mobile').removeClass('visible');
});




//Trap focus in menu when open

function navOffcanvasTrapFocus(e) {
  // add all the elements inside modal which you want to make focusable
  const isEscPressed = e.key === 'Escape',
        isTabPressed = e.key === 'Tab',
        $target = $(e.target);
        $trigger = e.data.trigger,
        $nav = e.data.nav;

  if (isEscPressed) {
    $trigger.removeClass('open');
    $nav.removeClass('open');
    $toggleTIElements = $nav.find('.toggle-tabindex');
    $toggleTIElements.attr('tabindex', '-1');
    $(document).off('keydown', navOffcanvasTrapFocus);

    if ($trigger.hasClass("block-scroll")) {
      $('html').toggleClass('disable-scroll');
      $('#overlay-mobile').toggleClass('visible');
    }
  }

  if (!isTabPressed) {
    return;
  }

  const focusableCssQuery = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
        $focusableElements = $nav.find(focusableCssQuery);
        $visiblefocusableElements = $focusableElements.filter(function() {
          if ($(this).is(':visible') && $(this).attr('tabindex') != '-1') {
            return $(this);
          }
        });
        $firstFocusableElement = $visiblefocusableElements.first();// $nav.find(focusableCssQuery + ':first');
        $lastFocusableElement = $visiblefocusableElements.last(); // $nav.find(focusableCssQuery + ':last');


  if (e.shiftKey) { // if shift key pressed for shift + tab combination
    if ($target.is($firstFocusableElement)) {
      $lastFocusableElement.focus(); // add focus for the last focusable element
      e.preventDefault();
    }
  } else { // if tab key is pressed
    if ($target.is($lastFocusableElement)) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
      // $firstFocusableElement.focus(); // add focus for the first focusable element
      $firstFocusableElement.focus();
      e.preventDefault();
    }
  }

}

function searchKeys(e) {
  // add all the elements inside modal which you want to make focusable
  let isEscPressed = e.key === 'Escape'; 
  let isTabPressed = e.key === 'Tab';

  if (isEscPressed) {
    closeSearch();
  }

  if (!isTabPressed) {
    return;
  }

  if (e.shiftKey) { // if shift key pressed for shift + tab combination
    if (document.activeElement === firstFocusableElement) {
      lastFocusableElement.focus(); // add focus for the last focusable element
      e.preventDefault();
    }
  } else { // if tab key is pressed
    if (document.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
      firstFocusableElement.focus(); // add focus for the first focusable element
      e.preventDefault();
    }
  }
}
;
///////////////////////Site Search Dropdown///////////////////////

(function() {
  const searchTrigger = document.getElementById('search--trigger'),
        searchDiv = document.querySelector('.search--site-search');
  
  function closeSearch() {
    searchTrigger.classList.remove('open');
    searchTrigger.setAttribute('aria-label', 'Open search input');
    searchDiv.classList.remove('search-open');
    window.removeEventListener('click', onDocumentClick);
  }

  const disableFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      element.setAttribute('tabindex', -1);
    });
  }

  const onDocumentClick = (e) => {
    if (!e.target.closest('.search--site-search')) {
      closeSearch();
    }
  }

  const toggleFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      let tabindex = (element.getAttribute('tabindex') === 0 ) ? -1 : 0;
      element.setAttribute('tabindex', tabindex);
    });
  }

  searchTrigger.addEventListener('click', (e) => {
    if (searchTrigger.classList.contains('open')) {
      closeSearch();
    }
    else {
      searchTrigger.classList.add('open');
      searchTrigger.setAttribute('aria-label', 'Close search input');
      searchDiv.classList.add('search-open');
      toggleFocusableElements();
      window.addEventListener('click', onDocumentClick);
    }
  });
})();



(function() {
	const   d = document;
    var     accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
	        setAccordionAria,
            switchAccordion,
            introLinks = d.querySelectorAll('.intro-link'),
            touchSupported = ('ontouchstart' in window),
            pointerSupported = ('pointerdown' in window),
            //buttonToggles = d.querySelectorAll('.rocket-buttons .buttons li a'),
            rocket = d.getElementById('blockchain');

    // Remove focus outline for users not using keyboard only
    handleFirstTab = function(e) {
        if (e.keyCode === 9) { // the "I am a keyboard user" key (tab)
            document.body.classList.add('user-is-tabbing');
            window.removeEventListener('keydown', handleFirstTab);
        }
    }
    window.addEventListener('keydown', handleFirstTab);
  
    skipClickDelay = function(e) {
        e.preventDefault();
        e.target.click();
    }

	setAriaAttr = function(el, ariaType, newProperty) {
		el.setAttribute(ariaType, newProperty);
	};
	setAccordionAria = function(el1, el2, expanded) {
		switch(expanded) {
            case "true":
                setAriaAttr(el1, 'aria-expanded', 'true');
                setAriaAttr(el2, 'aria-hidden', 'false');
                break;
            case "false":
                setAriaAttr(el1, 'aria-expanded', 'false');
                setAriaAttr(el2, 'aria-hidden', 'true');
                break;
            default:
                break;
		}
    };

    // Set focus for accessibility
    /*setFocus = function(element) {
        element.focus();
        console.log(document.activeElement);
        if (element === document.activeElement){ // Checking if the target was focused
            return false;
        } else {
            element.tabIndex = -1; // Adding tabindex for elements not focusable
            element.focus(); // Setting focus
         }
    }*/

    function scrollToItem(e) {
        e.preventDefault();
        scrollToElement = this.getAttribute('href');

        d.querySelector(scrollToElement).scrollIntoView({ 
            behavior: 'smooth' 
        });

        triggerOpen(scrollToElement);
    }
    
    // Open accordions when rocket component clicked
    switchAccordion = function(e) {
        e.preventDefault();

        var thisAnswer = e.target.parentNode.parentNode.nextElementSibling, //The expanded text
            thisQuestion = e.target.parentNode; //The link element that has been clicked

        // This is a keypress event, set the vars to accomondate for event bubbling
        if (e.target.classList.contains('js-accordionTrigger')) {
            thisAnswer = e.target.parentNode.nextElementSibling;
            thisQuestion = e.target;
        }

        //setFocus(thisQuestion);

        if (thisAnswer.classList.contains('is-collapsed')) {
            setAccordionAria(thisQuestion, thisAnswer, 'true');
            thisQuestion.scrollIntoView({ 
                behavior: 'smooth',
                block: 'start'
            });
        } else {
            setAccordionAria(thisQuestion, thisAnswer, 'false');
        }

  	    thisQuestion.classList.toggle('is-collapsed');
  	    thisQuestion.classList.toggle('is-expanded');
		thisAnswer.classList.toggle('is-collapsed');
		thisAnswer.classList.toggle('is-expanded');
        thisAnswer.classList.toggle('animateIn');
    };

    triggerOpen = function(elem) {
        elem = d.getElementById(elem.replace('#',''));

        setTimeout(function() {
            elem.querySelector('.js-accordionTrigger').click();
         }, 600);
    }
    
    // Add event listeners to rocket accordion components and bottom buttons
	for (var i=0, len=accordionToggles.length; i<len; i++) {
	    /*if (touchSupported) {
            accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
            buttonToggles[i].addEventListener('touchstart', expandAccordion, false);
        }
        if (pointerSupported) {
            accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
            buttonToggles[i].addEventListener('pointerdown', expandAccordion, false);
        }*/
        accordionToggles[i].addEventListener('click', switchAccordion, false);
        accordionToggles[i].addEventListener('keydown', function(e) {
            var key = e.which.toString();
            
            if (key.match(/13|32/)) {
                switchAccordion(e);
            }
        }, false);

        accordionToggles[i].setAttribute('data-element', [i]);
    }

    // Add event listeners for links in intro paragraph
    introLinks.forEach(link => {
        link.addEventListener('click', scrollToItem);
    });
})();
