$(document).ready(moveElements);
$(window).on('resize', moveElements);

// Constant top positioning px for nav elements
const NAV_TOP_ORIG = 20;

function moveElements() {
    if ($('#alert').hasClass('active')) {
        // Element vars
        const mainWrapper = $('#main-wrapper');
        const navTrigger = $('#nav--full .nav--full-trigger');
        const navOverlay = $('#nav--full .nav--full-overlay');
        const menuCircleTrans = $('#nav--full .menu-circle-transition');

        // Height of alert box, including padding and margins
        let alertHeight = $('#alert').innerHeight();

        // Reposition fixed elements in window to accommodate for alert height
        $(mainWrapper).css('margin-top', alertHeight + 'px');
        $(menuCircleTrans).css('top', alertHeight + 20);
        $(navTrigger).css('top', calcTopPosition(alertHeight));
        $(navOverlay).css('top', alertHeight);

        // Change header logo positioning depening on mobile or desktop widths
        if (checkSize() == "mobile") {
            $('#header .header--logo').css('top', alertHeight);
        } else {
            $('#header .header--logo').css('top', 0);
        }

        // Reset all margins and top positions when alert is closed
        $('#alert--dismiss').on('click', function(e) {
            e.preventDefault();
            $(mainWrapper).css('margin-top', 0);
            $(navTrigger).css('top', NAV_TOP_ORIG + 'px');
            $(menuCircleTrans).css('top', 20);
            $('#header .header--logo').css('top', 0);
            $(navOverlay).css('top', 0);
            return false;
        });
    }
}

function calcTopPosition(alertHeight) {
    return (NAV_TOP_ORIG + alertHeight) + 'px';
}

function checkSize() {
    if ($('#header .header--logo').css('position') == 'fixed') {
        return "mobile";
    } else {
        return "desktop";
    }
}
;
