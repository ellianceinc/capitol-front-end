///////////////////////Site Search Dropdown///////////////////////

(function() {
  const searchTrigger = document.getElementById('search--trigger'),
        searchDiv = document.querySelector('.search--site-search');
  
  function closeSearch() {
    searchTrigger.classList.remove('open');
    searchTrigger.setAttribute('aria-label', 'Open search input');
    searchDiv.classList.remove('search-open');
    window.removeEventListener('click', onDocumentClick);
  }

  const disableFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      element.setAttribute('tabindex', -1);
    });
  }

  const onDocumentClick = (e) => {
    if (!e.target.closest('.search--site-search')) {
      closeSearch();
    }
  }

  const toggleFocusableElements = () => {
    let elements = document.querySelectorAll('#search--main input, #search--main button');
    elements.forEach((element) => {
      let tabindex = (element.getAttribute('tabindex') === 0 ) ? -1 : 0;
      element.setAttribute('tabindex', tabindex);
    });
  }

  searchTrigger.addEventListener('click', (e) => {
    if (searchTrigger.classList.contains('open')) {
      closeSearch();
    }
    else {
      searchTrigger.classList.add('open');
      searchTrigger.setAttribute('aria-label', 'Close search input');
      searchDiv.classList.add('search-open');
      toggleFocusableElements();
      window.addEventListener('click', onDocumentClick);
    }
  });
})();
