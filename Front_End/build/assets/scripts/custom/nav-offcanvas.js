///////////////////////Nav Offcanvas///////////////////////
//http://web-accessibility.carnegiemuseums.org/code/accordions/
/*jslint browser: true*/
/*global $, jQuery, alert*/


var navButtons = $('#nav--main li button');

function navToggle() {
  // setting the correct active flyout menu item.
  var $activenavs;
  if (window.location.pathname === "/") {
    $activenavs = $('#nav--full #nav--main > li:nth-child(1) > ul > li:nth-child(1) > a');
  } else {
    $activenavs = $('#nav--full li a[href*="' + window.location.pathname + '"], #nav--full div.callouts a[href*="' + window.location.pathname + '"]');
  }
  $activenavs.each(function(idx, el) {
    $el = $(el);
    $el.addClass('current-page');
    var $mainnavParent = $el.parents('#nav--main > li');
    if ($mainnavParent[0]) {
      var $mainnavParentBtn = $($mainnavParent.find('button'));
      $mainnavParentBtn.attr('aria-expanded', true);
      $mainnavParentBtn.next('ul').css('display', 'block');
      $mainnavParentBtn.next('ul').attr('aria-hidden', "false");
      checkNavOthers($mainnavParentBtn[0]);
    }
  });
  // add the button click support
  $('#nav--main li button').on('click', function(e) {
    $control = $(this);

    navContent = $control.attr('aria-controls');
    checkNavOthers($control[0]);

    isAriaExp = $control.attr('aria-expanded');
    newAriaExp = (isAriaExp == "false") ? "true" : "false";
    $control.attr('aria-expanded', newAriaExp);

    isAriaHid = $('#' + navContent).attr('aria-hidden');
    if (isAriaHid == "true") {
      $(this).next('ul').attr('aria-hidden', "false");
      $(this).next('ul').css('display', 'block');
    } else {
      $(this).next('ul').attr('aria-hidden', "true");
      $(this).next('ul').css('display', 'none');
    }
  });
};


//close if others are open and close them
function checkNavOthers(elem) {
  for (var i = 0; i < navButtons.length; i++) {
    if (navButtons[i] != elem) {
      if (($(navButtons[i]).attr('aria-expanded')) == 'true') {
        $(navButtons[i]).attr('aria-expanded', 'false');
        content = $(navButtons[i]).attr('aria-controls');
        $('#' + content).attr('aria-hidden', 'true');
        $('#' + content).css('display', 'none');
        $('#' + content).parent().removeClass('active');
      }
    }
  }
};


//call this function on page load
$(document).ready(function() {
  navToggle();
});


//close accorions by default
function checkInitialNavState() {
  for (var i = 0; i < navButtons.length; i++) {
    if (($(navButtons[i]).attr('aria-expanded')) == 'false') {
      $(navButtons[i]).next('ul').css('display', 'none');
    }
  }
};
checkInitialNavState();









//Menu Open/Close button

//Toggle Scrip
$('.trigger').click(function() {
  var $this = $(this),
    notThis = $this.hasClass('open'),
    $thisNav = $('#' + $this.attr("rel"));

  //open the nav
  $this.toggleClass('open');
  $thisNav.toggleClass('open');
  if ($(this).hasClass("block-scroll")) {
    $('html').toggleClass('disable-scroll');
    $('#overlay-mobile').toggleClass('visible');
  }
  $toggleTIElements = $thisNav.find('.toggle-tabindex');
  $toggleTIElements.attr('tabindex', function(index, currentTI) {
    return ((currentTI - 1) % 2) + 0;
  });
  if ($(this).hasClass('open')) {
    $(this).find('.screen-reader-text').text('Close menu');
    //add event listener
    $(document).on('keydown', {trigger: $this, nav: $thisNav}, navOffcanvasTrapFocus);
  }
  else {
    $(this).find('.screen-reader-text').text('Open menu');
    //remove event listener
    $(document).off('keydown', navOffcanvasTrapFocus);
  }
});
//Close button
$('.offcanvas-close-button').click(function() {
  $('.triggered-area, .trigger').removeClass('open');
  $('html').removeClass('disable-scroll');
  $('#overlay-mobile').removeClass('visible');
});




//Trap focus in menu when open

function navOffcanvasTrapFocus(e) {
  // add all the elements inside modal which you want to make focusable
  const isEscPressed = e.key === 'Escape',
        isTabPressed = e.key === 'Tab',
        $target = $(e.target);
        $trigger = e.data.trigger,
        $nav = e.data.nav;

  if (isEscPressed) {
    $trigger.removeClass('open');
    $nav.removeClass('open');
    $toggleTIElements = $nav.find('.toggle-tabindex');
    $toggleTIElements.attr('tabindex', '-1');
    $(document).off('keydown', navOffcanvasTrapFocus);

    if ($trigger.hasClass("block-scroll")) {
      $('html').toggleClass('disable-scroll');
      $('#overlay-mobile').toggleClass('visible');
    }
  }

  if (!isTabPressed) {
    return;
  }

  const focusableCssQuery = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
        $focusableElements = $nav.find(focusableCssQuery);
        $visiblefocusableElements = $focusableElements.filter(function() {
          if ($(this).is(':visible') && $(this).attr('tabindex') != '-1') {
            return $(this);
          }
        });
        $firstFocusableElement = $visiblefocusableElements.first();// $nav.find(focusableCssQuery + ':first');
        $lastFocusableElement = $visiblefocusableElements.last(); // $nav.find(focusableCssQuery + ':last');


  if (e.shiftKey) { // if shift key pressed for shift + tab combination
    if ($target.is($firstFocusableElement)) {
      $lastFocusableElement.focus(); // add focus for the last focusable element
      e.preventDefault();
    }
  } else { // if tab key is pressed
    if ($target.is($lastFocusableElement)) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
      // $firstFocusableElement.focus(); // add focus for the first focusable element
      $firstFocusableElement.focus();
      e.preventDefault();
    }
  }

}

function searchKeys(e) {
  // add all the elements inside modal which you want to make focusable
  let isEscPressed = e.key === 'Escape'; 
  let isTabPressed = e.key === 'Tab';

  if (isEscPressed) {
    closeSearch();
  }

  if (!isTabPressed) {
    return;
  }

  if (e.shiftKey) { // if shift key pressed for shift + tab combination
    if (document.activeElement === firstFocusableElement) {
      lastFocusableElement.focus(); // add focus for the last focusable element
      e.preventDefault();
    }
  } else { // if tab key is pressed
    if (document.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
      firstFocusableElement.focus(); // add focus for the first focusable element
      e.preventDefault();
    }
  }
}
;
