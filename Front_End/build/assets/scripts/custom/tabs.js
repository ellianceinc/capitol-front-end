///////////////////////Tabs///////////////////////
// $(function() {
//     var index = 0;
//     var $tabs = $('button.tabOLD');
  
//     $tabs.bind ({
//       // on keydown,
//       // determine which tab to select
//       keydown: function(ev){
//         var LEFT_ARROW = 37;
//         var UP_ARROW = 38;
//         var RIGHT_ARROW = 39;
//         var DOWN_ARROW = 40;
  
//         var k = ev.which || ev.keyCode;
  
//         // if the key pressed was an arrow key
//         if (k >= LEFT_ARROW && k <= DOWN_ARROW){
//           // move left one tab for left and up arrows
//           if (k == LEFT_ARROW || k == UP_ARROW){
//             if (index > 0) {
//               index--;
//             }
//             // unless you are on the first tab,
//             // in which case select the last tab.
//             else {
//               index = $tabs.length - 1;
//             }
//           }
  
//           // move right one tab for right and down arrows
//           else if (k == RIGHT_ARROW || k == DOWN_ARROW){
//             if (index < ($tabs.length - 1)){
//               index++;
//             }
//             // unless you're at the last tab,
//             // in which case select the first one
//             else {
//               index = 0;
//             }
//           }
  
//           // trigger a click event on the tab to move to
//           $($tabs.get(index)).click();
//           ev.preventDefault();
//         }
//       },
  
//       // just make the clicked tab the selected one
//       click: function(ev) {
//         ev.preventDefault();
//         index = $.inArray(this, $tabs.get());
//         setFocus();
//       }
//     });
  
//     var setFocus = function(){
//       // undo tab control selected state,
//       // and make them not selectable with the tab key
//       // (all tabs)
//       $tabs.attr(
//       {
//         tabindex: '-1',
//         'aria-selected': 'false'
//       }).removeClass('selected');
  
//       // hide all tab panels.
//       $('.tab-panel').removeClass('current');
  
//       // make the selected tab the selected one, shift focus to it
//       $($tabs.get(index)).attr(
//       {
//         tabindex: '0',
//         'aria-selected': 'true'
//       }).addClass('selected').focus();
  
//       // handle parent <li> current class (for coloring the tabs)
//       $($tabs.get(index)).parent().siblings().removeClass('current');
//       $($tabs.get(index)).parent().addClass('current');
  
//       // add a current class also to the tab panel
//       // controlled by the clicked tab
//       $($($tabs.get(index)).attr('data-target')).addClass('current');
//     };
//   });
  
  (function() {
    let index = 0;
    const arrows = {
      left: 37,
      up: 38,
      right: 39,
      down: 40
    };
    const tabs = document.querySelectorAll('button.tab');

    const setFocus = (newIndex, focus = false) => {
      currentTab = tabs[index];
      currentPanel = document.getElementById(currentTab.getAttribute('aria-controls'));
      newTab = tabs[newIndex];
      newPanel = document.getElementById(newTab.getAttribute('aria-controls'));

      currentTab.setAttribute('tabindex', '-1');
      currentTab.setAttribute('aria-selected', 'false');
      currentTab.classList.remove('selected');
      currentPanel.classList.remove('current');

      newTab.setAttribute('tabindex', '0');
      newTab.setAttribute('aria-selected', 'true');
      newTab.classList.add('selected');
      if (focus) { newTab.focus(); }
      newPanel.classList.add('current');

      index = newIndex;
    }

    tabs.forEach((tab, i) => {
      tab.addEventListener('click', (e) => {
        setFocus(i);
      });
      tab.addEventListener('keydown', (e) => {
        const k = e.which || e.keyCode;
        console.log(Object.values(arrows).includes(k));
  
        if (Object.values(arrows).includes(k)){
          e.preventDefault();
          let newIndex;
          if (k == arrows.left || k == arrows.up){
            newIndex = (index > 0) ? index - 1 : tabs.length - 1;
          }  
          else if (k == arrows.right || k == arrows.down){
            newIndex =  (index < (tabs.length - 1)) ? index + 1 : 0;
          }
          setFocus(newIndex, true);
        }
      });
    });
  })();
