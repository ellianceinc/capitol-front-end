#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

ts=`date +"%Y%m%d_%H%M%S"`
echo $ts

rsync --exclude-from=$DIR/../ci-resources/rsync-excludes.txt --delete -brltvz --backup-dir=/rsync-backups/capitol-front-end/$ts $DIR/../build client-scus-lamp-pip.southcentralus.cloudapp.azure.com::htmlroot/capitol-front-end
